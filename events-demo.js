var events = require("events")


var eventEmitter = new events.EventEmitter();

var lis1 = () => { console.log("listerer1 executed")}

var list2 = () => { console.log("listerer2 executed")}

eventEmitter.addListener("connection",lis1);

eventEmitter.on("connection",list2)
eventEmitter.emit("connection");

eventEmitter.removeListener('connection', list1);
console.log("Listner1 will not listen now.");

eventEmitter.emit('connection');
